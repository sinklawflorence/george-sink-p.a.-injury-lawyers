We are a personal injury law firm with locations all across South Carolina and Georgia. We are proud to represent car accident and injured victims who are not receiving their due compensation from the big insurance companies. George Sink and his team of attorneys will fight for you.

Address: 181 East Evans Street, Suite 310, Florence, SC 29506, USA

Phone: 843-474-2700

Website: https://www.sinklaw.com/locations/south-carolina/florence/
